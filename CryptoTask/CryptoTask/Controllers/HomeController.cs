﻿using ClosedXML.Excel;
using CryptoTask.Services.Interfaces;
using CryptoTask.Web.Models;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Grid;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTask.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICryptoService _cryptoService;
        private static List<CoinVM> tempDataStorage = new List<CoinVM>();

        public HomeController(ILogger<HomeController> logger, ICryptoService cryptoService)
        {
            _logger = logger;
            _cryptoService = cryptoService;
        }

        public async Task<IActionResult> Index(string input)
        {
            //if (pageSize == 0)
            //    pageSize = 20;

            var listCoins = await _cryptoService.GetCoins(input);

            var listCoinsVM = listCoins.Adapt<IEnumerable<CoinVM>>();

            tempDataStorage = listCoinsVM.ToList();

            return View(listCoinsVM);
        }

        public IActionResult ExportToExcel()
        {

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("News");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Coin Name";
                worksheet.Cell(currentRow, 2).Value = "Rank";
                worksheet.Cell(currentRow, 3).Value = "Symbol";
                worksheet.Cell(currentRow, 4).Value = "Supply";
                worksheet.Cell(currentRow, 5).Value = "Price USD";

                foreach (var article in tempDataStorage)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = article.id;
                    worksheet.Cell(currentRow, 2).Value = article.rank;
                    worksheet.Cell(currentRow, 3).Value = article.symbol;
                    worksheet.Cell(currentRow, 4).Value = article.supply;
                    worksheet.Cell(currentRow, 5).Value = article.priceUsd;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "crypto.xlsx");
                }

            }
        }

        public IActionResult ExportToPdf()
        {
            PdfDocument doc = new PdfDocument();

            PdfPage page = doc.Pages.Add();

            PdfGrid pdfGrid = new PdfGrid();

            pdfGrid.DataSource = tempDataStorage;
            pdfGrid.Draw(page, new Syncfusion.Drawing.PointF(10, 10));

            MemoryStream stream = new MemoryStream();
            doc.Save(stream);

            stream.Position = 0;

            doc.Close(true);

            string contentType = "application/pdf";

            string fileName = "Output.pdf";

            return File(stream, contentType, fileName);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

      
        public IActionResult Privacy()
        {
            return View();
        }
    }
}
