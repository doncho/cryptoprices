﻿using CryptoTask.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoTask.Services.Interfaces
{
    public interface ICryptoService
    {
        public Task<IList<Coin>> GetCoins(string input);
    }
}
