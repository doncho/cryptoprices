﻿using CryptoTask.Data.Models;
using CryptoTask.Services.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoTask.Services
{
    public class CryptoService : ICryptoService
    {

        private const string ApiKey = "AIzaSyCO6N1zMLpRG_Lz-9WT3bQmo59f99Hrd3E";
        private readonly IHttpClientFactory clientFactory;

        public CryptoService(IHttpClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
        }

        public async Task<IList<Coin>> GetCoins(string input)
        {
            var client = this.clientFactory.CreateClient();

            var response = await client.GetAsync($"http://api.coincap.io/v2/assets?" 
                + $"&input={input}");

            var responseAsString = await response.Content.ReadAsStringAsync();

            var jObj = JObject.Parse(responseAsString);

            IList<JToken> resultList = jObj["data"].Children().ToList();

            IList<Coin> listCoins = new List<Coin>();

            foreach (JToken item in resultList)
            {
                Coin coin = item.ToObject<Coin>();
                listCoins.Add(coin);
            }

            if(input != default)
            {
                listCoins = listCoins.Where(x => x.id.Contains(input)).ToList();
            }

            return listCoins;
        }

    }
}
