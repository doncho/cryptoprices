﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoTask.Web.Models
{
    public class CoinVM
    {
        public string id { get; set; }
        public int rank { get; set; }
        public string symbol { get; set; }
        public string name { get; set; }
        public double supply { get; set; }
        public double? maxSupply { get; set; }
        public double marketCapUsd { get; set; }
        public double volumeUsd24Hr { get; set; }
        public double priceUsd { get; set; }
        public double changePercent24Hr { get; set; }
    }
}

